package com.commexpert;

import android.content.Context;

import com.truefriend.corelib.commexpert.ExpertBaseRealProc;
import com.truefriend.corelib.commexpert.intrf.IRealDataListener;

public class ExpertRealProc extends ExpertBaseRealProc
{

  public ExpertRealProc(Context oContext)
  {
    super(oContext);
  }
  public void InitInstance(IRealDataListener listener)
  {
    super.InitInstance(listener);
  }
  public void SetRealData(int nBlockIndex,int nItemIndex,String strData)
  {
    super.SetRealData( nBlockIndex, nItemIndex, strData);
  }
  public void ClearInstance()
  {
    super.ClearInstance();
  }
  public void RequestReal(String strRealId,String strKey)
  {
    super.RequestReal(strRealId,strKey);
  }
  public void ReleaseReal(String strRealId,String strKey)
  {
    super.ReleaseReal(strRealId,strKey);
  }
  public String GetRealData(int nBlockIndex,int nItemIndex)
  {
      return super.GetRealData( nBlockIndex, nItemIndex);
  }
  public int GetAttrRealData(int nBlockIndex, int nItemIndex)
  {
    return super.GetAttrRealData( nBlockIndex,  nItemIndex);
  }
  public void SetShowTrLog(boolean  bShowTRLog )
  {
    super.SetShowTrLog(bShowTRLog );
  }
}
