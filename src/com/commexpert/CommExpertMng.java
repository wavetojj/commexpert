package com.commexpert;

import java.util.ArrayList;

import com.truefriend.corelib.commexpert.CommBaseExpertMng;
import com.truefriend.corelib.commexpert.intrf.IExpertInitListener;
import com.truefriend.corelib.commexpert.intrf.IExpertLoginListener;
import com.truefriend.corelib.shared.ItemMaster.ItemCode;

public class CommExpertMng extends CommBaseExpertMng {

//  public  static void InitActivity(Activity activity)
//  {
//  }
//  public void InitCommExpert(Context context)
//  {
//  }
  public void SetInitListener(IExpertInitListener listener)
  {
    super.SetInitListener(listener);
  }
  public void SetLoginListener(IExpertLoginListener listener)
  {
    super.SetLoginListener(listener);
  }
//  public  CommExpertMng getInstance()
//  {
//
//  }
  public CommExpertMng()
  {
    super();
  }
  public void StartLogin( String strUserId, String strUserPW,String strCertPW )
  {
    super.StartLogin(  strUserId,  strUserPW, strCertPW );
  }
  public void Close()
  {
    super.Close();
  }
  public String GetLoginUserID( )
  {
    return super.GetLoginUserID( );
  }
  public int GetAccountSize()
  {
    return super.GetAccountSize();
  }
  public String GetAccountNo(int nIndex)
  {
    return super. GetAccountNo( nIndex);
  }
  public String GetAccountCode(int nIndex)
  {
    return super.GetAccountCode(nIndex);
  }
  public String GetAccountName(int nIndex)
  {
    return super.GetAccountName( nIndex);
  }
  public void SetCurrAccountInfo(String strAccount ,String strAccountCode )
  {
    super.SetCurrAccountInfo( strAccount , strAccountCode );
  }
  public  ArrayList<ItemCode> GetKospiCodeList()
  {
    return super.GetKospiCodeList();
  }
  public  ArrayList<ItemCode> GetKosdaqCodeList()
  {
    return super.GetKosdaqCodeList();
  }
  public  void  SetDevSetting(String strDev)
  {
    super.SetDevSetting(strDev);
  }
}
