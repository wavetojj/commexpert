package com.commexpert;

import com.truefriend.corelib.commexpert.ExpertBaseTranProc;
import com.truefriend.corelib.commexpert.intrf.ITranDataListener;
import android.content.Context;

public class ExpertTranProc extends ExpertBaseTranProc {

  public ExpertTranProc(Context oContext)
  {
    super(oContext);
  }
  public void ClearInstance()
  {
    super.ClearInstance();
  }
  public void InitInstance(ITranDataListener listener)
  {
    super.InitInstance(listener);
  }
  public void SetSingleData(int nBlockIndex, int nItemIndex,String strData)
  {
    super.SetSingleData( nBlockIndex,  nItemIndex, strData);
  }
  public void SetMultiData(int nBlockIndex,int nItemIndex,String strData,int nIndex)
  {
    super.SetMultiData( nBlockIndex, nItemIndex, strData, nIndex);
  }
  public String GetSingleData(int nBlockIndex,int nItemIndex)
  {
    return super.GetSingleData( nBlockIndex, nItemIndex);
  }
  public String GetMultiData(int nBlockIndex,int nItemIndex, int nDataIndex)
  {
    return  super.GetMultiData( nBlockIndex, nItemIndex,  nDataIndex);
  }
  public int GetAttrSingleData(int nBlockIndex,int nItemIndex)
  {
    return super.GetAttrSingleData(nBlockIndex,nItemIndex);
  }
  public int GetAttrMultiData(int nBlockIndex,int nItemIndex, int nDataIndex)
  {
    return super.GetAttrMultiData( nBlockIndex, nItemIndex,  nDataIndex);
  }
  public String GetEncryptPassword(String sValue)
  {
    return super.GetEncryptPassword( sValue);
  }
  public void SetCertType(int nType)
  {
    super.SetCertType(nType);
  }
  public int RequestData(String strTrCode )
  {
    return super.RequestData( strTrCode );
  }
  public int RequestNextData(String strTrCode )
  {
    return super.RequestNextData(strTrCode );
  }
  public int GetValidCount(int nBlockIndex)
  {
    return super.GetValidCount(nBlockIndex);
  }
  public void SetShowTrLog(boolean  bShowTRLog )
  {
    super.SetShowTrLog(bShowTRLog );
  }
}
